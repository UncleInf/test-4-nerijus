package lt.asw.tomas.test;

import lt.asw.tomas.test.services.LocalPetsService;
import lt.asw.tomas.test.services.PetsService;

public class TestApp {

    private PetsService petsService;

    public TestApp() {
        this.petsService = new LocalPetsService();
    }

    public void run() {
        System.out.println(petsService.getAllPets());
        petsService.removeByName("rikis");
        System.out.println(petsService.getAllPets());
    }
}
