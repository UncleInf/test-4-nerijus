package lt.asw.tomas.test.entities;

import java.util.Objects;

public class Pet extends BaseEntity {

    private String name;
    private String age;
    private String owner;

    public Pet(String name) {
        this(name, null, null);
    }

    public Pet(String name, String age, String owner) {
        this.name = name;
        this.age = age;
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return Objects.equals(name, pet.name) &&
                Objects.equals(owner, pet.owner);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, owner);
    }

    @Override
    public String toString() {
        return "Pet{" +
                "name='" + name + '\'' +
                '}';
    }
}
