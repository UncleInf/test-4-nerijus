package lt.asw.tomas.test.services;

import lt.asw.tomas.test.entities.Pet;

import java.util.List;

public interface PetsService {

    List<Pet> getAllPets();
    void removeByName(String name);

}
