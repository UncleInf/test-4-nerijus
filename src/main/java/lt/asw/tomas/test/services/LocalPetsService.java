package lt.asw.tomas.test.services;

import lt.asw.tomas.test.entities.Pet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LocalPetsService implements PetsService {

    private static List<Pet> pets = new ArrayList<>(Arrays.asList(
            new Pet("brisius"),
            new Pet("rikis"),
            new Pet("pukis")
    ));

    @Override
    public List<Pet> getAllPets() {
        return LocalPetsService.pets;
    }

    @Override
    public void removeByName(String name) {
        pets.remove(this.getIndexByPetName(name));
    }

    private int getIndexByPetName(String name) {
        int index = 0;
        for (Pet pet : pets) {
            if (pet.getName().equalsIgnoreCase(name)) {
                break;
            }
            index += 1;
        }
        return index;
    }

}
